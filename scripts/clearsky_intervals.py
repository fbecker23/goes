#!/usr/bin/env phyton
"""
SCRIPT FUNCTION
Make csv file wich contains clear sky intervals
"""
import os, sys
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib.patches import Rectangle
from matplotlib.dates import DayLocator, DateFormatter, drange
import matplotlib.gridspec as gridspec

# ---------------------------------------------------------------------------- #
# Set folders
dirfile = f"{os.environ['OUTDIR']}"
figdir  = f"{os.environ['FIGDIR']}"
datadir = f"{os.environ['DATADIR']}"

# ---------------------------------------------------------------------------- #
# Mask file (execute tata.py script previously)
mask       = xr.open_dataset(f"{datadir}/landsea_mask_example.nc")
nodos_land = (mask.lsm==0).sum().values
nodos_sea  = (mask.lsm==1).sum().values

# ---------------------------------------------------------------------------- #
# Open CSKY file
ds = xr.open_dataset(f"{dirfile}/ABI_GOES16.L2.CSKY.nc")

# Make an area index of clear sky over sea only
nsea = ds["BCM"].where(mask.lsm==1).sum(dim=["x","y"])/nodos_sea
df   = pd.DataFrame(nsea.to_dataframe()["BCM"])
df.columns = ["BCM"]
df.index   = df.index.round("H")
# Add column "counts"
df["counts"] = range(0, len(df))

# Definition of clear sky intervals using percetile values set as 50
inferior  = np.percentile(nsea.values, 50)
clear_sky = df[df.BCM<inferior].reset_index()

# Find clear cases in a row (every 3 hours)
filt    = clear_sky.loc[clear_sky["counts"].diff() == 1].rename(columns={"t":"groups"})
breaks  = filt["groups"].diff() != pd.Timedelta("3H")
groups  = breaks.cumsum()

# Build dataframe containing the clear sky cases (every 3 hours)
casos = pd.concat([clear_sky.loc[clear_sky["counts"].diff() == 1], groups], axis=1).drop("counts", axis=1)

# Looking for those cases that exceed dc hours of clear sky
ndays = 2	# number of days we consider clear sky window
res   = 3 	# 3 hour dataset resolution
dc    = 24/res * ndays # period clear sky 
time_bounds = []
for i in range(1,casos.groups.max()+1):
    if len(casos.t[casos.groups==i])>=dc:
        time_bounds.append(pd.DataFrame({"duration":[len(casos.t[casos.groups==i])*res], "start":[casos.t[casos.groups==i].iloc[0]], "end":[casos.t[casos.groups==i].iloc[-1]]}))

time_bounds = pd.concat(time_bounds).reset_index(drop=True)

print(f"Number of cases: {len(time_bounds):.0f}")
print(f"Max duration (hours) of the clear sky interval: {time_bounds['duration'].max():.0f}")

time_bounds.to_csv(f"{datadir}/clearsky_intervals_example.csv", sep=",", header=False, index=False)
