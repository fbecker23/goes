#!/usr/bin/env python
import glob, s3fs, os
import numpy as np
import xarray as xr
import pandas as pd

# load vars from config. file
tmpdir  = f"{os.environ['OUTDIR']}/tmp_sst"
os.makedirs(tmpdir, exist_ok = True)
datadir = f"{os.environ['DATADIR']}"

# Clear Sky intervals. Open file
csi = pd.read_csv(f"{datadir}/clearsky_intervals_example.csv", names=["duration", "start", "end"])

# Keep dates from clear sky intervals file. Only read first line from file
fechas = pd.DataFrame({"dates":pd.date_range(start=pd.to_datetime(csi.loc[0]["start"]), end=pd.to_datetime(csi.loc[0]["end"]), freq="H")})

#==============================================================================#
# AMAZON repository information
# https://noaa-goes16.s3.amazonaws.com/index.html
bucket_name = 'noaa-goes16'
product_name = 'ABI-L2-SSTF'
fs = s3fs.S3FileSystem(anon=True)

for fecha0 in fechas.dates:
    # Download
    files = fs.ls(bucket_name+'/'+product_name+'/'+fecha0.strftime('%Y')+'/'+fecha0.strftime('%j')+'/'+fecha0.strftime('%H')+'/')
    fname = files[0]
    fs.download(fname,f'{tmpdir}/ABI_GOES16.{fecha0.strftime("%Y%m%dT%H%M%S")}.L2.SST.nc')

