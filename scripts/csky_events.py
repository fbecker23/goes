#!/usr/bin/env phyton
"""
Compute plot showind Clear and Cloudy events
"""
import os, sys
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, DateFormatter, MonthLocator, WeekdayLocator, YearLocator
from scipy.signal import find_peaks

# ---------------------------------------------------------------------------- #
# Set folders
dirfile = f"{os.environ['OUTDIR']}"
figdir  = f"{os.environ['FIGDIR']}"
datadir = f"{os.environ['DATADIR']}"

# ---------------------------------------------------------------------------- #
# Mask file (execute tata.py script previously)
mask       = xr.open_dataset(f"{datadir}/landsea_mask_example.nc")
nodos_land = (mask.lsm==0).sum().values
nodos_sea  = (mask.lsm==1).sum().values

# ---------------------------------------------------------------------------- #
# Open CSKY file
ds = xr.open_dataset(f"{dirfile}/ABI_GOES16.L2.CSKY.nc")

# ---------------------------------------------------------------------------- #
# Make an area index of clear sky over sea only
nsea  = ds["BCM"].where(mask.lsm==1).sum(dim=["x","y"])/nodos_sea

# ---------------------------------------------------------------------------- #
# Running mean to detect events
freq            = 8     # sample 3 hourly resolution, 8 per day
per_long        = 7    # low pass period in days
per_short       = 1     # short filter in days
df_nsea         = pd.DataFrame(nsea.to_dataframe()["BCM"])
df_nsea.index   = df_nsea.index.round("H")
df_nsealongterm = df_nsea.rolling(freq*per_long, center=True).mean()
df_nsea         = 0.5 + (df_nsea-df_nsealongterm).rolling(freq*per_short, center=True).mean()

# Apply find_peaks function to find cloudy events
peaks, properties  = find_peaks(df_nsea["BCM"], prominence=0.5, width=freq*per_short)

# ---------------------------------------------------------------------------- #
# Features to compute cloudy and clear days
ntimes = 4  # Consecutive times to consider cloudy or clear sky event
# Percentile
q_c, ch_c = 0.65, ntimes   # Cloudy
q_d, ch_d = 0.35, ntimes   # Clear sky

# Define dataframe
df           = pd.DataFrame(nsea.to_dataframe()["BCM"])
df.columns   = ["BCM"]
df.index     = df.index.round("H")
df["counts"] = 1

# ---------------------------------------------------------------------------- #
# COMPUTE CLOUDY DAYS
# Upper threshold (cloudy)
df_cloudy = df[df.BCM>df["BCM"].quantile(q_c)]
# Sum cloud hours
cdf       = df_cloudy["counts"].resample("1D").sum()
# Cloudy day
cloudy_day = pd.DataFrame(cdf[cdf>=ch_c])
cloudy_day = cloudy_day.rename(columns={"counts":"n"})
cloudy_day["cloudy"] = 1

# ---------------------------------------------------------------------------- #
# COMPUTE CLEAR DAYS
# Lower threshold (cloudy)
df_clear = df[df.BCM <= df["BCM"].quantile(q_d)]
# Sum clear hours
ddf      = df_clear["counts"].resample("1D").sum()
# Clear day
clear_day = pd.DataFrame(ddf[ddf>=ch_d])
clear_day = clear_day.rename(columns={"counts":"n"})
clear_day["clear"] = 1

# ---------------------------------------------------------------------------- #
# Merge dataframes with cloudy and clear days
dfmerge           = pd.concat([clear_day, cloudy_day], axis=1)
dfmerge["events"] = dfmerge[["clear", "cloudy"]].sum(axis=1)

# ---------------------------------------------------------------------------- #
# Plot
fig, ax = plt.subplots(1, 1, figsize=(12,8), facecolor='w', edgecolor='k', sharex=True)
fig.subplots_adjust(top=0.95, bottom=0.075, left=0.075, right=0.95)
# Subset
ax.set_title('Cloudy and clear events', x=0.1, y=1., fontsize = 15)
# cloudy
ax.bar(dfmerge.index, dfmerge.cloudy, pd.Timedelta("1D"), color=(128/255, 128/255, 128/255), zorder=1, label='Cloudy day')
# clear
ax.bar(dfmerge.index, dfmerge.clear, pd.Timedelta("1D"), color=(135/255, 206/255, 235/255), zorder=1, label='Clear day')
# Running mean
ax.plot(df_nsea.index, df_nsea.BCM, c="k", lw=1.5, label="Running mean")
ax.vlines(x=df_nsea.index[peaks], ymin = df_nsea.BCM[peaks]-properties["prominences"], ymax = df_nsea.BCM[peaks], color = "k", ls="dashed", lw=1.5)
ax.hlines(y=properties["width_heights"], xmin=df_nsea.index[properties["left_ips"].astype(int)], xmax=df_nsea.index[properties["right_ips"].astype(int)], color = "k", ls="dashed", lw=1.5)
ax.legend(prop={'size': 12}, loc="upper left")
ax.set_ylim([0, 1.2])
#ax.set_xlim(pd.to_datetime(["20230314", "20230502"]))
ax.xaxis.set_major_locator(WeekdayLocator(interval = 1))
ax.xaxis.set_minor_locator(DayLocator(interval = 1))
ax.xaxis.set_major_formatter(DateFormatter('%d/%m\n %Y'))
ax.yaxis.set_tick_params(labelsize = 12)
ax.xaxis.set_tick_params(labelsize = 12)
ax.grid(True, which = "major", axis = "both", color = "k", ls = "-", lw = 0.2)#, c = "0.5")
ax.grid(True, which = "minor", axis = "both", color = "k", ls = "-", lw = 0.1)#, c = "0.75")
ax.tick_params(which = 'major', width = 0.50, length = 10)
ax.tick_params(which = 'minor', width = 0.25, length = 10)
ax.tick_params(axis='both', labelsize=12)
ax.set_ylabel("cloudy pixels/total pixels", fontsize=12)

#plt.show()
plt.savefig(f"{figdir}/csky_events.png", bbox_inches='tight')

