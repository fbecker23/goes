#!/usr/bin/env phyton
"""
Hovmoller
ABI SST over San Matias Gulf (GSM) during clear sky interval
"""

import glob, os, sys
import geopandas as gpd
import shapely as shp
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import cartopy.feature as cfeature

# ---------------------------------------------------------------------------- #
# Set folders
dirfile = f"{os.environ['OUTDIR']}"
figdir  = f"{os.environ['FIGDIR']}"
datadir = f"{os.environ['DATADIR']}"

# ========================================================== #
# Set lat/lon box and subset the data
lonMin, lonMax, latMin, latMax = -66.0, -62.0, -43.0, -40.0

# ---------------------------------------------------------------------------- #
# Borders
boundary_highres = cfeature.NaturalEarthFeature('physical', 'coastline', '10m') # NaturalEarthFeature
linestrings      = gpd.GeoDataFrame(geometry=[i for i in boundary_highres.geometries()])
# LineStrings borders
borders          = gpd.GeoDataFrame(geometry=gpd.GeoSeries(linestrings.geometry), crs=4326)
# Polygons borders
polygons         = gpd.GeoDataFrame(geometry=gpd.GeoSeries(shp.ops.polygonize(linestrings.geometry)), crs=4326)

#==============================================================================#
# SST ABI/GOES-16
#==============================================================================#
abisst        = xr.open_dataset(f"{dirfile}/ABI_GOES16.L2.SST.nc")
# QFLAGS
mask_abi      = abisst.DQF<=0            # good_quality_qf
abisst_subset = abisst.assign(SST=abisst["SST"]-273.16).where(mask_abi)

#==============================================================================#
# GSMz => Associated to GSM Zone Front
#==============================================================================#
dl        = 100 # Resolution of transect points
GSMz_poly = gpd.GeoDataFrame(geometry=gpd.points_from_xy(np.linspace(-64.15, -63.05, dl), -41.32 * np.ones(dl)).buffer(0.025), crs="EPSG:4326").dissolve().explode(index_parts=False).reset_index(drop=True)

#==============================================================================#
# Grouping bins
slots     = 0.05
to_bin    = lambda x: np.floor(x / slots) * slots      # bin space

#==============================================================================#
# Plot

times = pd.to_datetime(abisst_subset["t"].values)
GSMz = []

for time in times:
    abisst_filter = abisst_subset.sel(t=time)
    sst = abisst_filter.to_dataframe()[["lat", "lon", "SST"]].dropna().reset_index(drop=True)
    sst = gpd.GeoDataFrame(sst, geometry=gpd.points_from_xy(sst.lon, sst.lat), crs="EPSG:4326")
    GSMz_sst = gpd.overlay(sst, GSMz_poly, keep_geom_type=False, how='intersection')
    GSMz_sst["lon"] = to_bin(GSMz_sst.lon)
    GSMz_sst   = GSMz_sst.groupby("lon").mean(numeric_only=True)
    GSMz_sst   = GSMz_sst.reset_index()
    GSMz_sst["dSST"] = (GSMz_sst["SST"].diff()/(GSMz_sst["lon"].diff()* 111 * np.cos(np.deg2rad(GSMz_sst["lat"])))).abs()
    GSMz_sst["time"] = time
    GSMz.append(GSMz_sst.set_index(["time","lon"]).to_xarray())

GSMZ = xr.concat(GSMz, dim="time")

#==============================================================================#
# Miscellaneous
minimo   = np.floor(GSMZ["SST"].min()).values
maximo   = np.ceil(GSMZ["SST"].max()).values
bounds1  = np.linspace(minimo,maximo,51)
ncolors1 = len(bounds1) - 1
cmap1    = plt.cm.get_cmap('turbo', ncolors1)
norm1    = colors.BoundaryNorm(boundaries=bounds1, ncolors=ncolors1)
bounds2  = np.linspace(0,0.2,51)
ncolors2 = len(bounds2) - 1
cmap2    = plt.cm.get_cmap('jet', ncolors2)
norm2    = colors.BoundaryNorm(boundaries=bounds2, ncolors=ncolors2)

#==============================================================================#
fig, (ax1,ax2) = plt.subplots(1, 2, figsize = (9,12), sharey=True)
fig.subplots_adjust(top=0.85, bottom=0.1, left=0.1, right=0.99, wspace=0.05)
fig.suptitle(f'San Matias Gulf Meridional Front (GSMF)\n {times[0].round("H"):%b/%Y}', fontsize=15)

ax1.set_title('SST\n (degree C)', fontsize=15)
fill = ax1.pcolormesh(GSMZ["lon"], GSMZ["time"], GSMZ["SST"], cmap = cmap1, norm=norm1)
ax1.set_ylabel('Time', fontsize=13)
ax1.set_xlabel('Longitude', fontsize=13)
ax1.grid()
ax1.invert_yaxis()
cb = fig.colorbar(fill, ax=ax1, orientation='horizontal', extend="both", label='degree_C', shrink=1.)
cb.ax.tick_params(labelsize=10)
cb.set_ticks(bounds1)
cb.ax.locator_params(nbins=10)

ax2.set_title('Absolute gradient of SST\n (|degree C/km|)', fontsize=15)
fill = ax2.pcolormesh(GSMZ["lon"], GSMZ["time"], GSMZ["dSST"], cmap = cmap2, norm=norm2)
ax2.set_xlabel('Longitude', fontsize=13)
ax2.grid()
cb     = fig.colorbar(fill, ax=ax2, orientation='horizontal', extend="both", label='|degree C/km|', shrink=1.)
cb.ax.tick_params(labelsize=10)
cb.set_ticks(bounds2)
cb.ax.locator_params(nbins=10)

#plt.show()
plt.savefig(f"{figdir}/sst_hovmoller_GSMF_{time:%Y%m%d%H}.png", bbox_inches='tight')
