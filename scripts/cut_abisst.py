#!/usr/bin/env python

# ---------------------------------------------- #
# CUT FILES
# ---------------------------------------------- #

import glob, os, sys
import numpy as np
import xarray as xr
import pandas as pd
from datetime import datetime
from dask.distributed import Client, LocalCluster

#==============================================================================#
# The math for this function was taken from
# https://makersportal.com/blog/2018/11/25/goes-r-satellite-latitude-and-longitude-grid-projection-algorithm

def calc_latlon(ds) :
  x = ds.x
  y = ds.y
  goes_imager_projection = ds.goes_imager_projection
  x,y = np.meshgrid(x,y)
  r_eq = goes_imager_projection.attrs["semi_major_axis"]
  r_pol = goes_imager_projection.attrs["semi_minor_axis"]
  l_0 = goes_imager_projection.attrs["longitude_of_projection_origin"] * (np.pi/180)
  h_sat = goes_imager_projection.attrs["perspective_point_height"]
  H = r_eq + h_sat
  a = np.sin(x)**2 + (np.cos(x)**2 * (np.cos(y)**2 + (r_eq**2 / r_pol**2) * np.sin(y)**2))
  b = -2 * H * np.cos(x) * np.cos(y)
  c = H**2 - r_eq**2
  r_s = (-b - np.sqrt(b**2 - 4*a*c))/(2*a)
  s_x = r_s * np.cos(x) * np.cos(y)
  s_y = -r_s * np.sin(x)
  s_z = r_s * np.cos(x) * np.sin(y)
  lat = np.arctan((r_eq**2 / r_pol**2) * (s_z / np.sqrt((H-s_x)**2 +s_y**2))) * (180/np.pi)
  lon = (l_0 - np.arctan(s_y / (H-s_x))) * (180/np.pi)
  ds = ds.assign_coords({"lat":(["y","x"],lat),"lon":(["y","x"],lon)})
  ds.lat.attrs["units"] = "degrees_north"
  ds.lon.attrs["units"] = "degrees_east"
  return ds


#==============================================================================#
# Folder Datasets
datadir = f"{os.environ['OUTDIR']}/tmp_sst"
outdir  = f"{os.environ['OUTDIR']}"
data    = f"{os.environ['DATADIR']}"

# Open Mask (Remember execute gen_mask.py to make mask)
mask  = xr.open_dataset(f"{data}/landsea_mask_example.nc")
dx = slice(mask.x.min().values, mask.x.max().values)
dy = slice(mask.y.max().values, mask.y.min().values)

# ========================================================== #
# open & cut files
files = sorted(glob.glob(f"{datadir}/*nc"))

# Defino esta funcion que corrige los metadatos de ABI (Los unifica)
def preprocess_SST(ds, dx=dx, dy=dy):
    keys        = list(ds.keys())
    keys_chnged = [ks.replace("Reynolds", "Ancillary") for ks in keys]
    rename      = dict(zip(keys, keys_chnged))
    ds          = ds.rename(rename)
    # ROI #
    ds          = ds.sel(x=dx, y=dy)
    ds          = calc_latlon(ds)
    return ds

def cutoff(files=files, outdir=outdir):
    # ---------------------------------------------------------------------------- #
    # Abro cluster
    # ---------------------------------------------------------------------------- #
    ICORE = 10
    GB_per_core = 3
    cluster = LocalCluster(n_workers = ICORE, threads_per_worker = 1, memory_limit = f'{GB_per_core}GiB')
    client = Client(cluster)
    # Open multiple files
    ds    = xr.open_mfdataset(files,
                             concat_dim = 't',
                             combine    = 'nested',
                             preprocess = preprocess_SST,
                             parallel   = True)
    # Save Multiples files
    ds.to_netcdf(f"{outdir}/ABI_GOES16.L2.SST.nc")
    # ---------------------------------------------------------------------------- #
    # Cierro cluster
    # ---------------------------------------------------------------------------- #
    client.close()
    cluster.close()


if __name__ == "__main__":
    cutoff()

