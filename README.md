# goes_sst

This repository arises as a final practical work of a UBA/FCEN faculty project. Using GOES-16 ABI data here we evaluate the diurnal variability of the meridional tidal front over San Matias Gulf

Sofía Muñoz(1), Violeta Valdeolmillos(1) and Matías De Oto(1,2)

(1) Universidad de Buenos Aires
(2) Servicio Meteorológico Nacional

## Previous steps

### 1. Into your personal machine, open bash terminal and clone or fork this repository

```console
pepe@ocean:~$ git clone https://gitlab.com/mdeotoproschle/goes_sst.git
```

### 2. Clone goes16_sst environment using environments.yaml file

```console
pepe@ocean:~$ conda env create --name goes16_sst --file=environment.yml
```

### 3. Activate goes16_sst environment

```console
pepe@ocean:~$ source activate goes16_sst
```

## Steps to execute the code

### Go to folder scripts

### 1. Configure your path folders in config.ini file

```console
pepe@ocean:~$ vi config.ini 
pepe@ocean:~$ source config.ini 
```

### 2. Download one month of GOES16 ABI Clear Sky Mask (CSKY) from AWS

```console
pepe@ocean:~$ python down_abimask.py "20200201"
```
Explanation 

download_abimask.py download 3 hourly data files of ABI GOES16 CSKY from AWS server using "FullDisk" domain. The files will be located in the folder ./download/tmp_mask

Note: AWS repository contains information of ABI GOES-16 from 2020. Download takes a while and each mask file is about 2 M

To check if file is OK, you could use ncviewer

```console
pepe@ocean:~$ ncview $MAINDIR/downloads/tmp_mask/ABI_GOES16.20200201T000000.L2.CSKY.nc
```

### 3.a. Cut files into a Region of interest (ROI)

Explanation

Previously it is necessary make a mask of the ROI, so first, modify the file **gen_mask.py** introducing the limits latitudes and longitudes of our ROI. In this example, ROI is the San Matias Gulf over the northern patagonian.

NOTE: **gen_mask.py** uses one file of ABI mask downloaded previously.

```console
pepe@ocean:~$ python gen_mask.py
```

### 3.b. Cut files into a Region of interest (ROI)

```console
pepe@ocean:~$ python cut_abimask.py
```
### 4. Compute and plot Clear and Cloudy events over ROI

Explanation

Function **csky_events.py** generate an index of cloudy days over ROI. This index are based into two criteria. 
(a) *objetive* criteria: compute spatial ratio between cloudy pixels and total pixels in marine area (using mask, see 3.a.)  
(b) *subjetive* criteria: define percentile of cloudy and clear sky and define the period to consider cloudy or clear sky event 
Figures are saved in ./figs folder

```console
pepe@ocean:~$ python csky_events.py
```

<figure>
<img src="https://gitlab.com/mdeotoproschle/goes_sst/-/raw/main/figs/csky_events.png" align="center" >
<figcaption align = "center">
<b>Fig.1 - Cloudy and Clear sky index</b>
</figcaption>
</figure>

### 5. Make a csv file wich contains clear sky intervals over ROI 
 
Explanation

Function **clearsky_intervals.py** compute first the spatial ratio of clear sky over the marine region (using mask, see 3.a.). A percentile and period of interval is proposed (in a similar way to do in section 4.b.). This period of clear sky interval are saved into a .csv file, named **clearsky_intervals.csv** in data folder.

```console
pepe@ocean:~$ python clearsky_intervals.py
```

### 6. Download clear sky intervals of GOES-16 ABI SST data 

```console
pepe@ocean:~$ python down_abisst.py
```

Explanation

Function **down_abisst.py** download GOES-16 ABI SST data from AWS in full resolution using dates provides by the file **clearsky_intervals.csv**.

Note: Download takes a while and each file is about 30 M

To check if file is Ok, you could use ncviewer

```console
pepe@ocean:~$ ncview $MAINDIR/downloads/tmp_mask/<some_file_SST.nc>
```

### 7. Cut files into a Region of interest (ROI)

```console
pepe@ocean:~$ python cut_abisst.py
```

Explanation

In a similar way to what **cut_abimask.py** does, function cut_abisst.py cut "FullDisk" of SST data over ROI. This script uses the mask generated into section 3.a.

### 8. Plot fields and Hovmoller Diagram of the diurnal variability of the front

```console
pepe@ocean:~$ python sst_fields.py
pepe@ocean:~$ python sst_hovmoller.py
```

Explanation

Function **sst_fields.py** plot and save HOURLY SST figures over ROI into folder ./figs. On the other hand, **sst_hovmoller.py** plot the Hovmoller diagram of SST and the absolut gradient of SST over whole clear sky interval period


<figure>
<img src="https://gitlab.com/mdeotoproschle/goes_sst/-/raw/main/figs/sst_fields_2020021500.png" align="center" >
<figcaption align = "center">
<b>Fig.2 - SST field</b>
</figcaption>
</figure>

<figure>
<img src="https://gitlab.com/mdeotoproschle/goes_sst/-/raw/main/figs/sst_hovmoller_GSMF_2020021700.png" align="center" >
<figcaption align = "center">
<b>Fig.3 - Hovmoller Diagram</b>
</figcaption>
</figure>

### Acknowledgments

This project was made possible thanks to the efforts of the professors Silvia Romero, Laura Ruiz y Melina Martinez who teached the course [Oceanografia Satelital](http://www-atmo.at.fcen.uba.ar/programas/OceanografiaSatelital.pdf) at the Department of Atmospheric and Ocean Sciences (DCAO) of the FCEN of the University of Buenos Aires. Also thanks to the advice of Juan Pablo Pisoni.

